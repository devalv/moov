import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton  from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import {logout} from '../utils/Firebase.js';
import {Link} from 'react-router-dom';
import Exit from 'material-ui/svg-icons/action/exit-to-app';

export default class PageTitle extends Component {
    constructor(props){
        super(props);
        this.state={

        };
    }
    componentWillMount(){
        
    }
    render() {
        return (
            <AppBar 
            iconElementLeft={
                <Link to={this.props.isLoggedIn? "/admin":"/"}><FontIcon style={{margin: '10px 0 0',color: '#fff', verticalAlign:'middle'}} className="material-icons">home</FontIcon></Link>
            }
            
            title={this.props.title} 
            style={{marginBottom:30,backgroundColor: '#FF5722',color:'#fff',marginTop:0,marginLeft:0,marginRight:0,width:'100%' }} 
            iconElementRight = {
              <div>
              {
                this.props.isLoggedIn===true&&
                <div>
                {
                  this.props.hasBackButton===true&&
                    <FontIcon style={{display: 'inline-block',color: '#fff', verticalAlign:'middle'}} className="material-icons">arrow_back</FontIcon>
                }
                <FlatButton 
                  style={{display: 'inline-block',margin: '7px 0 0', color: '#fff'}} 
                  onClick={() => {
                    logout()
                  }}
                  labelPosition="before"
                  icon = {<Exit />}
                />
                </div>                
              }
              </div>
            } 
          />  
        );
    }
}


