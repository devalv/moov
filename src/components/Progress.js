import React, { Component } from 'react';
import '../utils/Style.css';
import CircularProgress from 'material-ui/CircularProgress';
import TextField from'material-ui/TextField';
import Paper from 'material-ui/Paper';
import PageTitle from './PageTitle';
import ImageUploader from 'react-images-upload';
import DatePicker from 'material-ui/DatePicker';
import PrintFotos from '../utils/PrintFotos';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
import {saveImages, saveInfo,updateObj,dateToString} from '../utils/Controller';
import {Redirect} from 'react-router-dom';

export default class Progress extends Component {
    constructor(props){
        super(props);
        this.state={
            data:null,
            isLoading:true,
            PushKey:null,
            Objetivo:'',
            pictures:[],
            progresos:null,
            add:false,
            Fecha:null,
            Altura:'',
            Peso:'',
            "Porcentaje de grasa":'',
            redirectBack:false,
        };
    }
    validateNumber = (number) =>{
        if(isNaN(number)){
          return false;
        } 
        return true;
    }
    onDrop = (picture) =>{
        let pictures = this.state.pictures;
        pictures.push(picture);
        this.setState({pictures});
    }
    saveNew = () =>{
        this.setState({isLoading:true})
        let self = this;
        let saved = saveImages(this.state.pictures,this.state.data.CI);
        setTimeout(()=>{
            let data = {
                Fecha:dateToString(self.state.Fecha),
                Altura:self.state.Altura,
                Peso:self.state.Peso,
                "Porcentaje de grasa":self.state["Porcentaje de grasa"],
                Fotos:saved
            }
            let sent = saveInfo(data,self.state.PushKey);
            if(sent){
                alert('Datos Agregados');
                self.setState({redirectBack:true})
            }  
            self.setState({isLoading:false}) 
        },3000)
    }
    updateObjetivo = () =>{
        let Objetivo = this.state.Objetivo;
        let result = updateObj(Objetivo,this.state.PushKey);
        if(result){
            alert('Objetivo Actualizado');
        } 
        console.log('result: ', result);
    }
    componentWillMount(){
        this.setState({Fecha:new Date()})
        if(this.props.location.data){
            let data = this.props.location.data[1];
            this.setState({
                data,
                isLoading:false,
                PushKey:this.props.location.data[0],
                Objetivo:data.Objetivo?data.Objetivo:'',
                progresos:data.progresos?data.progresos:null
            })
        }
    }
    changeAltura = (e) =>{
        this.setState({ Altura: e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorAltura:'',error:false});
            } else {
            this.setState({errorAltura:'Altura inválida',error:true});
            }
        }
        else{
            this.setState({errorAltura:'Correo Inválido',error:true});
        }
        return false;
    }
    changePeso =(e) =>{
        this.setState({ Peso: e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorPeso:'',error:false});
            } else {
            this.setState({errorPeso:'Peso inválido',error:true});
            }
        }
        else {
            this.setState({errorPeso:'Peso inválido',error:true});
        }
        return false;
    }
    handleFecha = (event, date) => {
        this.setState({Fecha: date});
    };
    addNew = () =>{
        this.setState({add:true})
    }
    render() {
        let progresos = this.state.progresos!==null&&
        Object.values(this.state.progresos).map((element,index)=>{
            return(
                <div key={index}>
                    <br /><br />
                    <Paper className="loginContainer" zDepth={1}>
                        <div>
                            <h3>"Evolución / Transformación"</h3>
                        </div>
                        <div>
                            <TextField 
                                fullWidth={true}
                                value={element.Fecha}
                                floatingLabelText="Fecha" 
                            />
                        </div>  
                        <div>
                            <TextField 
                                fullWidth={true}
                                value={element.Peso}
                                floatingLabelText="Peso" 
                            />
                        </div>
                        <div>
                            <TextField 
                                fullWidth={true}
                                value={element.Altura}
                                floatingLabelText="Altura" 
                            />
                        </div>  
                        <div>
                            <TextField 
                                fullWidth={true}
                                value={element['Porcentaje de grasa']}
                                floatingLabelText="Porcentaje de grasa" 
                            />
                        </div>
                        <div>
                            <PrintFotos fotos={element.Fotos}></PrintFotos>  
                        </div>
                    </Paper>
                    <br /><br />
                </div>
            )
        })
        if(this.state.redirectBack===true){
            return(
                <Redirect to="/main" />
            );
        }
        else{
        return this.state.isLoading === true ? <div className="content"><CircularProgress color='#FF5722' size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle title={'Progreso'}/>
                
                {
                    this.state.data!==null&&
                    <div>
                        <h3>{this.state.data.Nombres + ' '+this.state.data.Apellidos}</h3>
                        <br /><br />
                        <Paper className="loginContainer" zDepth={1}>
                            <div>
                                <TextField 
                                    fullWidth={true}
                                    value={this.state.Objetivo}
                                    onChange={e => this.setState({ Objetivo: e.target.value})} 
                                    floatingLabelText="Objetivo" 
                                    multiLine={true}
                                    rows={2}
                                />
                            </div>   
                            <div>
                            <br /><br />
                                <RaisedButton label="Actualizar Objetivo" onClick={this.updateObjetivo}/>
                            </div>
                        </Paper>
                        
                        <br /><br />
                        {progresos}
                        <br /><br />
                        {
                            this.state.add === true ?
                                <Paper className="loginContainer" zDepth={1}>
                                    <div>
                                        <h3>"Evolución / Transformación"</h3>
                                    </div>
                                    <div>
                                        <DatePicker 
                                            floatingLabelText="Fecha" 
                                            hintText="Fecha" 
                                            container="inline" 
                                            fullWidth={true}
                                            value={this.state.Fecha}
                                            onChange={this.handleFecha}
                                        />
                                    </div>  
                                    <div>
                                        <TextField 
                                            fullWidth={true}
                                            value={this.state.Peso}
                                            errorText={this.state.errorPeso}
                                            onChange={this.changePeso} 
                                            floatingLabelText="Peso (kg)" 
                                        />
                                    </div>
                                    <div>
                                        <TextField 
                                            fullWidth={true}
                                            value={this.state.Altura}
                                            errorText={this.state.errorAltura}
                                            onChange={this.changeAltura} 
                                            floatingLabelText="Altura (cm)" 
                                        />
                                    </div>  
                                    <div>
                                        <TextField 
                                            fullWidth={true}
                                            value={this.state['Porcentaje de grasa']}
                                            floatingLabelText="Porcentaje de grasa" 
                                            onChange={e => this.setState({ "Porcentaje de grasa": e.target.value})} 
                                        />
                                    </div>
                                    <div>
                                        <ImageUploader
                                            withIcon={true}
                                            buttonText='Subir Imagenes'
                                            onChange={this.onDrop}
                                            imgExtension={['.jpg','.jpeg', '.gif', '.png', '.gif']}
                                            maxFileSize={5242880}
                                            label='Subir imagen hasta de 5mb'
                                            withPreview={true}
                                        />
                                    </div>
                                    <div>
                                    <br /><br />
                                    <RaisedButton label="Agregar" onClick={this.saveNew}/>
                                    </div>
                                </Paper>
                                :
                                <RaisedButton label="Agregar Nuevo" onClick={this.addNew}/>
                        }
                    </div>
                }
            </div>
        );
    }
    }
}


