import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import logo from '../assets/logo.jpg';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import imgNuevo from '../assets/nuevo.png';
import imgUsuarios from '../assets/usuarios.png';
import StarBorder from 'material-ui/svg-icons/navigation/check';
import {Link} from 'react-router-dom';

const tilesData = [
    {
        img: imgUsuarios,
        title: 'Usuarios',
        author: 'Datos de todos tus Usuarios.',
        link:'main'
    },
    {
        img: imgNuevo,
        title: 'Nuevo Usuario',
        author: 'Agrega un nuevo usuario',
        link:'nuevo',
    }
  ];
  
  const styles = {
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
    },
    gridList: {
      width: 500,
      height: 400,
      overflowY: 'auto',
    },
  };

export default class Admin extends Component {
    constructor(props){
        super(props);
        this.state={
            
        };
    }
    startOver(){

    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        return(
            <div className="App">
                <PageTitle 
                title="Administración"
                isLoggedIn={true}
                />
                <div>
                    <img src={logo} width="200" alt="logo"/>
                </div>
                <div style={styles.root}>
                    <GridList
                    cellHeight={300}
                    style={styles.gridList}
                    >
                        <Subheader>Moov Gym</Subheader>
                        {tilesData.map((tile) => (
                        <GridTile
                        key={tile.img}
                        title={tile.title}
                        subtitle={<span><b>{tile.author}</b></span>}
                        actionIcon={<Link to={tile.link}><IconButton tooltip="Ir" ><StarBorder color="white"/></IconButton></Link>}
                        >
                        <img src={tile.img} alt="icon"/>
                        </GridTile>
                    ))}
                    </GridList>
                </div>
            </div>
        )
    }
}


