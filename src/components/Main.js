import React, { Component } from 'react';
import '../utils/Style.css';
import CircularProgress from 'material-ui/CircularProgress';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import PageTitle from './PageTitle';
import {getGymUsers,deleteUser} from '../utils/Controller';
import {Redirect} from 'react-router-dom'
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';

export default class Main extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading:false,
            users:[],
            detail:false,
            data:null,
            progress:false,
        };
    }
    startOver(){
        let self = this;
        getGymUsers().then(value=>{
            self.setState({users:Object.entries(value.toJSON())});
        }).catch(error=>{
            console.log('error: ', error);
        })
    }
    handleDetail=(data)=>{
        this.setState({detail:true,data})
    }
    handleProgress=(data)=>{
        this.setState({progress:true,data})
    }
    handleDelete=(data)=>{
        let deleted = deleteUser(data);
        console.log('deleted: ', deleted);
        if(deleted){
            alert('Usuario Eliminado');
            this.startOver();
        }
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let users = this.state.users.length>0&&
        this.state.users.map((data,index)=>{
            return(
                <TableRow key={index}>
                    <TableRowColumn>{data[1].CI}</TableRowColumn>
                    <TableRowColumn >{data[1].Nombres}</TableRowColumn>
                    <TableRowColumn>{data[1].Apellidos}</TableRowColumn>
                    <TableRowColumn>{data[1]['Fecha de Ingreso']}</TableRowColumn>
                    <TableRowColumn>
                        <IconButton iconClassName="material-icons" tooltip="Detalle" onClick={()=>this.handleDetail(data)}>pageview</IconButton>
                    </TableRowColumn>
                    <TableRowColumn>
                        <IconButton iconClassName="material-icons" tooltip="Progreso" onClick={()=>this.handleProgress(data)}>forward</IconButton>
                    </TableRowColumn>
                    <TableRowColumn>
                        <IconButton iconClassName="material-icons" tooltip="Progreso" onClick={()=>this.handleDelete(data)}>delete</IconButton>
                    </TableRowColumn>
                </TableRow>
            );    
        })
        if (this.state.detail === true) {
            return <Redirect to={{
                pathname: '/detail',
                data: this.state.data
              }}/> 
        }
        else if (this.state.progress===true){
            return <Redirect to={{
                pathname: '/progress',
                data: this.state.data
              }}/> 
        }
        else{
            return this.state.isLoading === true ? <div className="content"><CircularProgress color={"#FF5722"} size={100} thickness={7} /></div> : (
                <div className="App">
                    <PageTitle title={'Administración'} isLoggedIn={true}/>
                    <Paper className="loginContainer" zDepth={1}>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                                <TableHeaderColumn>CI</TableHeaderColumn>
                                <TableHeaderColumn>Nombres</TableHeaderColumn>
                                <TableHeaderColumn>Apellidos</TableHeaderColumn>
                                <TableHeaderColumn>F. Ingreso</TableHeaderColumn>
                                <TableHeaderColumn>Detalle</TableHeaderColumn>
                                <TableHeaderColumn>Progreso</TableHeaderColumn>
                                <TableHeaderColumn>Eliminar</TableHeaderColumn>
                            </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                            {users}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            );
        }
    }
}


