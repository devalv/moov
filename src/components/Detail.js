import React, { Component } from 'react';
import {editGymUser,dateToString} from '../utils/Controller';
import '../utils/Style.css';
import SelectField from 'material-ui/SelectField';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress';
import TextField from'material-ui/TextField';
import Paper from 'material-ui/Paper';
import DatePicker from 'material-ui/DatePicker';
import PageTitle from './PageTitle';

export default class Detail extends Component {
    constructor(props){
        super(props);
        this.state={
            data:null,
            isLoading:true,
            "Alergias o Intolerancias":"",
            Altura:'',
            "Antecedentes Patológicos Personales":'',
            Apellidos:'',
            CI:'',
            Correo:'',
            Dirección:'',
            "Fecha de Ingreso": '',
            "Fecha de Nacimiento":'',
            Genero:'',
            Nombres:'',
            "Número de Móvil":'',
            "Número de Telefono Fijo":'',
            Peso:'',
            PushKey:null,
        };
    }
    handleEdit=()=>{
        let toSave = {
            "Alergias o Intolerancias":this.state['Alergias o Intolerancias'],
            Altura:this.state.Altura,
            "Antecedentes Patológicos Personales":this.state["Antecedentes Patológicos Personales"],
            Apellidos:this.state.Apellidos,
            CI:this.state.CI,
            Correo:this.state.Correo,
            Dirección:this.state.Dirección,
            "Fecha de Ingreso": dateToString(this.state["Fecha de Ingreso"]),
            "Fecha de Nacimiento":dateToString(this.state["Fecha de Nacimiento"]),
            Genero:this.state.Genero,
            Nombres:this.state.Nombres,
            "Número de Móvil":this.state["Número de Móvil"],
            "Número de Telefono Fijo":this.state["Número de Telefono Fijo"],
            Peso:this.state.Peso,
        }
        let work = editGymUser(toSave,this.state.PushKey);
        if(work===true){
            alert('Cambios Realizados');
        }
    }
    componentWillMount(){
        if(this.props.location.data){
            let data = this.props.location.data[1];
            this.setState({
                data,
                PushKey:this.props.location.data[0],
                isLoading:false,
                "Alergias o Intolerancias":data["Alergias o Intolerancias"],
                Altura:data.Altura,
                "Antecedentes Patológicos Personales":data["Antecedentes Patológicos Personales"],
                Apellidos:data.Apellidos,
                CI:data.CI,
                Correo:data.Correo,
                Dirección:data.Dirección,
                "Fecha de Ingreso": new Date(data["Fecha de Ingreso"]),
                "Fecha de Nacimiento":new Date(data["Fecha de Nacimiento"]),
                Genero:data.Genero,
                Nombres:data.Nombres,
                "Número de Móvil":data["Número de Móvil"],
                "Número de Telefono Fijo":data["Número de Telefono Fijo"],
                Peso:data.Peso
            })
        }
    }
    handleFechaDeNacimiento = (event, date) => {
        this.setState({"Fecha de Nacimiento": date});
    };
    handleFechaDeIngreso = (event, date) => {
        this.setState({"Fecha de Ingreso": date});
    };
    validateNumber = (number) =>{
        if(isNaN(number)){
          return false;
        } 
        return true;
    }
    changeNombre = (e) =>{
        this.setState({ Nombres: e.target.value})
        if(e.target.value!==''){
          this.setState({errorNombre:'',error:false});
        } else {
          this.setState({errorNombre:'Nombre Inválido',error:true});
        }
        return false;
    }
    changeApellido = (e) =>{
        this.setState({ Apellidos: e.target.value})
        if(e.target.value!==''){
        this.setState({errorApellido:'',error:false});
        } else {
        this.setState({errorApellido:'Apellido Inválido',error:true});
        }
        return false;
    }
    changeCorreo=(e)=>{
        this.setState({ Correo: e.target.value})
        if(e.target.value!==''){
            this.setState({errorCorreo:'',error:false});
          } else {
            this.setState({errorCorreo:'Correo Inválido',error:true});
          }
          return false;
    }
    changeAltura = (e) =>{
        this.setState({ Altura: e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorAltura:'',error:false});
            } else {
            this.setState({errorAltura:'Altura inválida',error:true});
            }
        }
        else{
            this.setState({errorAltura:'Correo Inválido',error:true});
        }
        return false;
    }
    changeDireccion=(e)=>{
        this.setState({ Dirección: e.target.value})
        if(e.target.value!==''){
            this.setState({errorDireccion:'',error:false});
          } else {
            this.setState({errorDireccion:'Dirección Inválida',error:true});
          }
          return false;
    }
    changePeso =(e) =>{
        this.setState({ Peso: e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorPeso:'',error:false});
            } else {
            this.setState({errorPeso:'Peso inválido',error:true});
            }
        }
        else {
            this.setState({errorPeso:'Peso inválido',error:true});
        }
        return false;
    }
    changeCI = (e) =>{
        this.setState({ CI: e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorCI:'',error:false});
            } else {
            this.setState({errorCI:'Cédula inválida',error:true});
            }
        }else {
            this.setState({errorCI:'Cédula inválida',error:true});
        }
        return false;
    }
    changeFijo = (e) =>{
        this.setState({ "Número de Telefono Fijo": e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorFijo:'',error:false});
            } else {
            this.setState({errorFijo:'Número inválido',error:true});
            }
        }  else {
            this.setState({errorFijo:'Número inválido',error:true});
        }      
        return false;
    }
    changeCel=(e)=>{
        this.setState({ 'Número de Móvil': e.target.value})
        let number = e.target.value;
        if(e.target.value!==''){
            if(this.validateNumber(number)){
            this.setState({errorCel:'',error:false});
            } else {
            this.setState({errorCel:'Número de celular inválido',error:true});
            }
        } else {
            this.setState({errorFijo:'Número inválido',error:true});
        }  
        return false;
    }
    render() {
        return this.state.isLoading === true ? <div className="content"><CircularProgress color={"#FF5722"} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle title={'Detalle'}/>
                {
                    this.state.data!==null&&
                        <Paper className="loginContainer" zDepth={1}>
                            <h3>{this.state.Nombres + ' '+this.state.Apellidos}</h3>
                            <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.CI}
                        onChange={this.changeCI} 
                        floatingLabelText="CI" 
                        errorText={this.state.errorCI} 
                        />
                    </div>   
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Nombres}
                        errorText={this.state.errorNombre} 
                        onChange={this.changeNombre} 
                        floatingLabelText="Nombres"/>
                        
                    </div>
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Apellidos}
                        errorText={this.state.errorApellido}
                        onChange={this.changeApellido} 
                        floatingLabelText="Apellidos"/>
                    </div>
                    <div>
                        <DatePicker 
                        floatingLabelText="Fecha de Nacimiento" 
                        hintText="Fecha de Nacimiento" 
                        container="inline" 
                        fullWidth={true}
                        value={this.state['Fecha de Nacimiento']}
                        onChange={this.handleFechaDeNacimiento}
                        />
                    </div>
                    <div>
                        <SelectField
                        fullWidth={true}
                        floatingLabelText="Género"
                        value={this.state.Genero}
                        onChange={this.handleGenero}
                        style={{textAlign:'left'}}
                        >
                        <MenuItem value={'Femenino'} primaryText="Femenino" />
                        <MenuItem value={'Masculino'} primaryText="Masculino" />
                        </SelectField>
                    </div>
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Peso}
                        errorText={this.state.errorPeso}
                        onChange={this.changePeso} 
                        floatingLabelText="Peso (kg)" 
                        />
                    </div>     
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Altura}
                        errorText={this.state.errorAltura}
                        onChange={this.changeAltura} 
                        floatingLabelText="Altura (cm)" 
                        />
                    </div>     
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state['Antecedentes Patológicos Personales']}
                        onChange={e => this.setState({ 'Antecedentes Patológicos Personales': e.target.value})} 
                        floatingLabelText="Antecedentes Patológicos Personales" 
                        multiLine={true}
                        rows={2}
                        rowsMax={4}
                        style={{textAlign:'left'}}
                        />
                    </div>     
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state['Alergias o Intolerancias']}
                        onChange={e => this.setState({ 'Alergias o Intolerancias': e.target.value})} 
                        floatingLabelText="Alergias o Intolerancias" 
                        multiLine={true}
                        rows={2}
                        style={{textAlign:'left'}}
                        rowsMax={4}
                        />
                    </div>
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state["Número de Telefono Fijo"]}
                        onChange={this.changeFijo} 
                        floatingLabelText="Número de Telefono Fijo" 
                        errorText={this.state.errorFijo}
                        />
                    </div>     
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state['Número de Móvil']}
                        onChange={this.changeCel} 
                        floatingLabelText="Número de Móvil" 
                        errorText={this.state.errorCel}
                        />
                    </div>
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Correo}
                        onChange={this.changeCorreo} 
                        floatingLabelText="Correo" 
                        errorText={this.state.errorCorreo}
                        />
                    </div>     
                    <div>
                        <TextField 
                        fullWidth={true}
                        value={this.state.Dirección}
                        onChange={this.changeDireccion} 
                        floatingLabelText="Dirección" 
                        multiLine={true}
                        errorText={this.state.errorDireccion}
                        style={{textAlign:'left'}}
                        rows={2}
                        rowsMax={4}
                        />
                    </div>
                    <div>
                        <DatePicker 
                        floatingLabelText="Fecha de Ingreso" 
                        hintText="Fecha de Ingreso" 
                        container="inline" 
                        fullWidth={true}
                        value={this.state['Fecha de Ingreso']}
                        onChange={this.handleFechaDeIngreso}
                        />
                    </div>                                                
                            <div style={{padding:20}}>
                                <RaisedButton label="Modificar" onClick={this.handleEdit} />
                            </div>
                        </Paper>
                }
            </div>
        );
    }
}


