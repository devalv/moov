import React, { Component } from 'react';
import './Style.css';

export default class PrintFotos extends Component {
    constructor(props){
        super(props);
        this.state={
            fotos:[],
        };
    }
   
    componentWillMount(){
        if(this.props.fotos){
            let fotos = Object.values(this.props.fotos);
            this.setState({fotos});
        }
    }
    render() {
        if(this.state.fotos.length>0){
            let fotos = Object.values(this.state.fotos).map((element,index)=>{
                return(
                    <a href={element} target="_blank"><img key={index} src={element} alt="Fotos" className="fotos" /></a>
                )
            })
            return (
                <div className="App">
                    {fotos}
                </div>
            );
        }
        else{
            return '';
        }
    }
}


