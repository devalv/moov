import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBaYZhKx-fATMvjOc7HobkOFY6ESDGnNi8",
    authDomain: "moov-gym.firebaseapp.com",
    databaseURL: "https://moov-gym.firebaseio.com",
    projectId: "moov-gym",
    storageBucket: "moov-gym.appspot.com",
    messagingSenderId: "950806992543"
};
export const firebaseApp = firebase.initializeApp(config);
export const db = firebaseApp.database();
export const storage = firebaseApp.storage();
export const auth = firebaseApp.auth(); 
export const storageKey = 'KEY_FOR_LOCAL_STORAGE';
export const isAuthenticated = () => {
    return !!auth.currentUser || !!localStorage.getItem(storageKey);
}
export const logout = ()=>{
    auth.signOut();
}