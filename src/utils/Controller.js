import {db, storage} from './Firebase.js';

export function getGymUsers(){
    let ref=db.ref('gymUsers/');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function saveNewUserGym(data){
    let ref=db.ref('gymUsers/');
    ref.push(data);
    return true;
}
export function editGymUser(data,key){
    let ref=db.ref('gymUsers/'+key);
    ref.set(data);
    return true;
}
export function updateObj (data,key){
    let ref = db.ref('gymUsers/'+key+'/Objetivo');
    ref.set(data);
    return true;
}
export function saveImages(imagenes,name){
    let size = imagenes.length -1;
    let imgs = imagenes[size];
    console.log('imgs: ', imgs);
    let urls = [];
    imgs.map((element)=>{
        let today = new Date().getTime() + '';
        var storageRef = storage.ref(name).child(today);
        storageRef.put(element).then(function(snapshot) {
            storage.ref(snapshot.metadata.fullPath).getDownloadURL().then(function (url) {
                urls.push(url);
                console.log('url: ', url);
            });
        });
        return true;
    });
    return urls;
}
export function deleteUser(data){
    let key = data[0];
    console.log('key: ', key);
    db.ref('gymUsers/'+key ).remove();
    return true;
}
export function saveInfo(data,key){
    let ref=db.ref('gymUsers/'+key + '/progresos/');
    ref.push(data);
    return true;
}
export function todayDate(){
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
export function dateToString(dat){
    var d = new Date(dat),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}