import React, { Component } from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import './Style.css';
import Login from '../components/Login.js';
import Admin from '../components/Admin.js';
import Detail from '../components/Detail.js';
import Progress from '../components/Progress.js';
import Main from '../components/Main.js';
import NewUser from '../components/NewUser.js';
import {auth} from './Firebase.js';
import { Route, BrowserRouter, Redirect, Switch } from 'react-router-dom'

function PrivateRoute ({component: Component, authed, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => authed === true
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
      />
    )
  }
  
  function PublicRoute ({component: Component, authed, superuser, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => 
          authed === false? 
            <Component {...props} />
          :  
            <Redirect to='/admin' />
        }
      />
    )
  }

export default class Routes extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:true,
            authed:false,
            store:null,
        }
    }
    componentDidMount () {
        this.removeListener = auth.onAuthStateChanged((user) => {
            if (user) {
                localStorage.uid = user.uid;
                this.setState({
                    authed: true,
                    loading: false,
                })
            } else {
                this.setState({
                    authed: false,
                    loading: false,
                })
            }
        })
    }
    componentWillUnmount () {
        this.removeListener()
    }
    render() {
    return this.state.loading === true ? <div className="content"><CircularProgress color={"#FF5722"} size={100} thickness={7} /></div> : (
            <BrowserRouter>
                <Switch>
                    <PublicRoute authed={this.state.authed} path='/' exact component={Login} />
                    <PublicRoute authed={this.state.authed} path='/login' component={Login} />
                    <PrivateRoute authed={this.state.authed} path='/admin' component={Admin} />
                    <PrivateRoute authed={this.state.authed} path='/main' component={Main} />
                    <PrivateRoute authed={this.state.authed} path='/detail' component={Detail} />
                    <PrivateRoute authed={this.state.authed} path='/progress' component={Progress} />
                    <PrivateRoute authed={this.state.authed} path='/nuevo' component={NewUser} />
                    <Route render={() => <h6>Error 404 No Encontrado</h6>} />
                </Switch>
          </BrowserRouter>
          );
  }
}